import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard-view',
  templateUrl: './dashboard-view.component.html',
  styleUrls: ['./dashboard-view.component.css']
})
export class DashboardViewComponent implements OnInit {

  userDetails: any;
  constructor() { }

  ngOnInit() {
    this.userDetails = {
      id: localStorage.getItem('id'),
      name: localStorage.getItem('name'),
      password: localStorage.getItem('password'),
      course: localStorage.getItem('course'),
      city: localStorage.getItem('city'),
      state: localStorage.getItem('state'),
      country: localStorage.getItem('country')
    }
  }

}
