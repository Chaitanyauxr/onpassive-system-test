import {
  Component,
  OnInit
} from '@angular/core';

@Component({
  selector: 'app-register-view',
  templateUrl: './register-view.component.html',
  styleUrls: ['./register-view.component.css']
})
export class RegisterViewComponent implements OnInit {

     courseOptions: any ={
          showCount: true,
          defaultValue: 'B-tech',
          label: '',
          options: [
              {
                  value: 'B-tech',
                  count: 0,
                  disableTab: false
              },
              {
                    value: 'Degree',
                    count: 0,
                    disableTab: false
               },
               {
                    value: 'Diploma',
                    count: 0,
                    disableTab: false
               },
               {
                    value: 'Other',
                    count: 0,
                    disableTab: false
               }
          ]
     };

     userDetails = {
          id: '',
          name: '',
          password: '',
          course: '',
          city: '',
          state: '',
          country: ''
     };

     constructor() {}

     ngOnInit() {}

     courseSelected(data) {
          this.userDetails.course = data.value;
     }

     submitFormDetails() {
          if (this.userDetails.id !== '' && this.userDetails.name !== '' && this.userDetails.password !== '' && this.userDetails.course !== '' && this.userDetails.city !== '' && this.userDetails.state !== '' && this.userDetails.country !== '') {
               localStorage.setItem('id', this.userDetails.id);
               localStorage.setItem('name', this.userDetails.name);
               localStorage.setItem('password', this.userDetails.password);
               localStorage.setItem('course', this.userDetails.course);
               localStorage.setItem('city', this.userDetails.city);
               localStorage.setItem('state', this.userDetails.state);
               localStorage.setItem('country', this.userDetails.country);
               window.location.href = "/dashboard";
          } else {
               alert("All Fields are Mandatory");
          }

     }

}
