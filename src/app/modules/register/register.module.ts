import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RegisterRoutingModule } from './register-routing.module';
import { RegisterViewComponent } from './register-view/register-view.component';
import { FormsModule } from '@angular/forms';
import { DropdownComponent } from 'src/app/components/dropdown/dropdown.component';


@NgModule({
  declarations: [RegisterViewComponent, DropdownComponent],
  imports: [
    CommonModule,
    RegisterRoutingModule,
    FormsModule
  ]
})
export class RegisterModule { }
