import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    HostListener,
    ViewChild,
    ElementRef
} from '@angular/core';

@Component({
    selector: 'app-dropdown',
    templateUrl: './dropdown.component.html',
    styleUrls: ['./dropdown.component.css']
})
export class DropdownComponent implements OnInit {
    @Input() dropdownOptions: any;
    @Input() dropdownValue: any;
    @Output() dropdownValueSelected = new EventEmitter<any>();

    @ViewChild('dropdownTrigger', {
        static: false
    }) dropdownTrigger: ElementRef;
    @ViewChild('dropdownList', {
        static: false
    }) dropdownList: ElementRef;
    @ViewChild('dropdownInput', {
        static: false
    }) dropdownInput: ElementRef;

    constructor() {}

    ngOnInit() {
    }

    @HostListener('document:click', ['$event']) clickout($event) {
        if (this.dropdownTrigger) {
            if (this.dropdownTrigger.nativeElement.contains($event.target)) {
                this.dropdownList.nativeElement.classList.toggle('d-none');
                this.dropdownInput.nativeElement.classList.toggle('dropdown-open');
            } else {
                this.dropdownList.nativeElement.classList.add('d-none');
                this.dropdownInput.nativeElement.classList.remove('dropdown-open');
            }
        }
    }

    dropdownSelected(event) {
        this.dropdownValue = event.value;
        this.dropdownValueSelected.emit(event);
    }
}
